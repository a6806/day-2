import argparse
from typing import TextIO

FORWARD = 'forward'
DOWN = 'down'
UP = 'up'


class Submarine1:
    def __init__(self):
        self.position = (0, 0)

    def get_position(self):
        return self.position

    def process_command(self, command):
        command, x = command.split()
        x = int(x)
        h_pos, depth = self.position
        if command == FORWARD:
            self.position = (h_pos + x, depth)
        elif command == DOWN:
            self.position = (h_pos, depth + x)
        elif command == UP:
            self.position = (h_pos, depth - x)
        else:
            raise RuntimeError('Unknown command')


class Submarine2:
    def __init__(self):
        self.position = (0, 0)
        self.aim = 0

    def get_position(self):
        return self.position

    def process_command(self, command):
        command, x = command.split()
        x = int(x)
        h_pos, depth = self.position
        if command == FORWARD:
            self.position = (h_pos + x, depth + x * self.aim)
        elif command == DOWN:
            self.aim += x
        elif command == UP:
            self.aim -= x
        else:
            raise RuntimeError('Unknown command')


def part_1(input_file: TextIO):
    input_text = input_file.read()
    submarine = Submarine1()
    for command in input_text.split('\n'):
        submarine.process_command(command)
    h_pos, depth = submarine.get_position()
    return h_pos * depth


def part_2(input_file: TextIO):
    input_text = input_file.read()
    submarine = Submarine2()
    for command in input_text.split('\n'):
        submarine.process_command(command)
    h_pos, depth = submarine.get_position()
    return h_pos * depth


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 2')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
